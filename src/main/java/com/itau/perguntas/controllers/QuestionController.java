package com.itau.perguntas.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.itau.perguntas.models.Modelo;
import com.itau.perguntas.models.Question;
import com.itau.perguntas.repositories.QuestionRepository;


@RestController
public class QuestionController {
	static RestTemplate restTemplate = new RestTemplate();
	
	@Autowired
	QuestionRepository questionRepository;

		@RequestMapping(path="/question/random", method=RequestMethod.GET)
		public ResponseEntity<?> getIdRandom(){
			
     		List<Question> questions = (List<Question>) questionRepository.findAll();
     		
     		int random = (int)Math.ceil(Math.random() * questions.size()) -1 ;
			
    	    return ResponseEntity.status(200).body(questions.get(random));
		}
		
		
		@RequestMapping(method=RequestMethod.GET, path="/question/{id}")
		public ResponseEntity<?> getId(@PathVariable int id){
			return ResponseEntity.status(201).body(questionRepository.findById(id));
		}
		
		
		@RequestMapping(path="/question",method=RequestMethod.POST)
		public ResponseEntity<?> createQuestion(@RequestBody Question question){
			
			try {
				Question questionNew = questionRepository.save(question);
				return ResponseEntity.status(201).body(questionNew);
			}
			catch (Exception e) {
				return ResponseEntity.badRequest().build();
			}
		}

		@RequestMapping(path="/question/carga",method=RequestMethod.POST)
		public int buscaQuestions(){
			
			String url = "http://10.162.106.164:8000/all";
			RestTemplate rest = new RestTemplate();
			ResponseEntity<List<Modelo>> response = rest.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<Modelo>>() {});
			
			List<Modelo> questions = response.getBody();
			int count = 0;
			
			for (Modelo modelo : questions) {
				
				Question question = new Question();
				question.setTitle(modelo.getTitle());
				question.setCategory(modelo.getCategory());
				question.setOption1(modelo.getOption1());
				question.setOption2(modelo.getOption2());
				question.setOption3(modelo.getOption3());
				question.setOption4(modelo.getOption4());
				question.setAnswer(modelo.getAnswer());
				questionRepository.save(question);
				count +=1;
			}
			
			return count;
				
			}
			
		}			