package com.itau.perguntas.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.perguntas.models.Question;

public interface QuestionRepository extends CrudRepository<Question, Integer>  {
	public Optional<Question> findByid(int id);
}	